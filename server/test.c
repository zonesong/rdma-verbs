#include "rdma_IBV_Verbs.h"

#define RDMAMSGR "RDMA read operation "

int main(int argc, char *argv[])
{
    int rc = 1;
    char *verbs_buf = NULL;

    // 初始化
    if (init_verbs("enp59s0f0", "mlx5_0", 65536) < 0) {
        fprintf(stderr, "failed to post sr\n");
        goto main_exit;
    }

    verbs_buf = verbs_get_buffer();

    // 业务流程
    for (int i = 0; ; i++)
    {
        /* setup server buffer with read message */
        sprintf(verbs_buf, "%s %d", RDMAMSGR, i);
        fprintf(stdout, "Contents of server buffer: '%s'\n", verbs_buf);

        if (verbs_send() < 0) {
            fprintf(stderr, "failed to verbs_send\n");
            goto main_exit;
        }

        if (verbs_recv() < 0) {
            fprintf(stderr, "failed to verbs_recv\n");
            goto main_exit;
        }

	    if ((verbs_buf[0] & 0xFF) == 0x99) {
		    fprintf(stdout, "client has been finished\n");
		    break;
	    }

        fprintf(stdout, "[0x%02x]After Client Modified, Contents of server buffer: '%s'\n", verbs_buf[0], verbs_buf);
    }
    rc = 0;

main_exit:
    destroy_verbs();
    fprintf(stdout, "\ntest result is %d\n", rc);
    return rc;
}

