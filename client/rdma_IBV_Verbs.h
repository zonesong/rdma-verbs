#ifndef RDMA_IBV_VERBS_H
#define RDMA_IBV_VERBS_H

#ifdef __cplusplus
#extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <endian.h>
#include <byteswap.h>
#include <getopt.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <infiniband/verbs.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <linux/if.h>

// 初始化verbs环境
// eth_dev:以太网网卡     
// svr_ip:服务端以太网IP地址
// verbs_dev:IB网卡       
// size:verbs共享内存的大小为 64B*size
extern int init_verbs(const char *eth_dev, const char *svr_ip, const char *verbs_dev, size_t size);
// 销毁verbs资源
extern void destroy_verbs();
// 接收服务端数据
extern int verbs_recv();
// 发送回应消息到服务端
extern int verbs_send();
// 获取verbs共享内存地址，内存大小为64B的整倍数
extern char *verbs_get_buffer();

#ifdef __cplusplus
}
#endif

#endif
