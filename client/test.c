#include "rdma_IBV_Verbs.h"

#define RDMAMSGW "RDMA write operation FROM Client"

int main(int argc, char *argv[])
{
    int rc = 1;
    int i = 0;
    char *verbs_buf = NULL;

    struct timeval start_t, end_t;
    double total_t;

    if (init_verbs("enp59s0f1", "192.168.2.3", "mlx5_1", 65536)) {
            fprintf(stderr, "init verbs failed\n");
            rc = 1;
            goto main_exit;
    }
    gettimeofday(&start_t,NULL);

    verbs_buf = verbs_get_buffer();

    for (; i < 1000; i++)
    {
        /* after polling the completion we have the message in the client buffer too */
        fprintf(stdout, "Old Content Message is: '%s'\n", verbs_buf);

        // Every time get the wanted content, must calling verbs_recv first
        if (verbs_recv() < 0) {
            fprintf(stderr, "verbs receive failed\n");
            rc = 1;
            goto main_exit;
        }
        fprintf(stdout, "Current Contents of server's buffer: '%s'\n", verbs_buf);

        // 模拟业务操作
        sprintf(verbs_buf, "%s %d", RDMAMSGW, i);
        fprintf(stdout, "Now replacing it with: '%s'\n", verbs_buf);

        //sleep(2);  // 模拟应产生的延迟

        // give the server a back message
        if (verbs_send() < 0) {
            fprintf(stderr, "verbs send failed\n");
            rc = 1;
            goto main_exit;
        }
    }
    rc = 0;

    // beging finishing
    if (verbs_recv() < 0) {
        fprintf(stderr, "verbs receive failed\n");
        rc = 1;
        goto main_exit;
    }
    fprintf(stdout, "Current Contents of server's buffer: '%s'\n", verbs_buf);

    verbs_buf[0] = 0x99;
    if (verbs_send() < 0) {
        fprintf(stderr, "verbs send failed\n");
        rc = 1;
        goto main_exit;
    }

    gettimeofday(&end_t,NULL);
    if(end_t.tv_usec >= start_t.tv_usec)
        total_t = (double)(end_t.tv_sec - start_t.tv_sec)+(double)(end_t.tv_usec-start_t.tv_usec)/1000000.0 ;
    else
        total_t = (double)(end_t.tv_sec - start_t.tv_sec-1)+(double)(end_t.tv_usec+1000000.0-start_t.tv_usec)/1000000.0 ;
    printf("[Server->Client] Speed:---%lf MB/S---------\n", (double)4*1024*1024*i / total_t/ 1024 / 1024);

main_exit:
    destroy_verbs();
    fprintf(stdout, "\ntest result is %d\n", rc);
    return rc;
}
